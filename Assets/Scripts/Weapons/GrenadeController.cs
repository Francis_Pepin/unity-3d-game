using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeController : IWeaponController
{
    private Grenade grenade;

    public override void Start()
    {
        base.Start();
        grenade = GetComponent<Grenade>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        if (prop.ownerCamera == null)
        {
            return;
        }

        var rb = GetComponent<Rigidbody>();
        var ownerCamera = prop.ownerCamera;
        GetComponent<Prop>().owner.GetComponentInChildren<Inventory>().Drop();
        rb.AddForce(3500f * rb.mass * ownerCamera.transform.forward);
        grenade.isArmed = true;
    }
}
