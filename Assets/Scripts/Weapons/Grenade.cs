using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayerMask = 0;

    public bool isArmed;
    private Rigidbody rb;
    private ParticleSystem explosionParticles;

    private float radius = 10f;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        explosionParticles = GetComponentInChildren<ParticleSystem>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!isArmed)
        {
            return;
        }

        explosionParticles.Play();
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, radius, transform.up, 0.01f, playerLayerMask);
        foreach(var hit in hits)
        {
            var rb = hit.rigidbody;
            var direction = hit.collider.transform.position - transform.position;
            if (rb)
            {
                var distanceMultiplier = Mathf.Clamp((radius - direction.magnitude) / radius, 0f, 1f);
                rb.AddForce(2000f * direction * distanceMultiplier);
            }
        }

        isArmed = false;
        Destroy(transform.Find("Mesh").gameObject);
        Destroy(this.gameObject, 5f);
    }
}
