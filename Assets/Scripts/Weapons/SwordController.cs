﻿using System.Collections.Generic;
using UnityEngine;

public class SwordController : IWeaponController
{
    private float timeSinceSwing = 0f;
    private float swingAmount = 0.2f;
    private bool hasSwung = false;
    private Vector3 swingDirection = Vector3.zero;
    public Animator animator;

    public override void Start() 
    {
        base.Start();
        animator = GetComponentInChildren<Animator>();
        timeSinceSwing = 10f;
    }

    void Update()
    {
        timeSinceSwing += Time.deltaTime;

        if (!prop.isActive)
        {
            return;
        }

        if (timeSinceSwing > 3f)
        {
            if (Input.GetButton("Fire1"))
            {
                Swing();
            }
        }

        if (hasSwung && timeSinceSwing > swingAmount)
        {
            var rb = prop.owner.GetComponent<Rigidbody>();
            rb.velocity = 10f * swingDirection;
            hasSwung = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

    }

    private void Swing()
    {
        animator.SetTrigger("swing");
        timeSinceSwing = 0f;

        var rb = prop.owner.GetComponent<Rigidbody>();
        if (prop.owner && rb)
        {
            swingDirection = prop.ownerCamera.transform.forward;
            rb.AddForce(25000f * rb.mass * swingDirection);
            hasSwung = true;
        }
    }
}
