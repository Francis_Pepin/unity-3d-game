using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

public abstract class IWeaponController : NetworkBehaviour
{
    public LayerMask playerLayerMask = 0;
    protected Prop prop = null;

    public virtual void Start()
    {
        prop = GetComponent<Prop>();
    }
}
