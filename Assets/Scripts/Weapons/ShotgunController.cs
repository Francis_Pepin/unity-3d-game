using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunController : IWeaponController
{
    [SerializeField] private ParticleSystem bulletParticles = null;
    [SerializeField] private ParticleSystem fireParticles = null;
    [SerializeField] private ParticleSystem smokeParticles = null;

    private float maxDistance = 50f;
    private float timeSinceFire = 2f;
    private int bulletsShot = 0;

    void Update()
    {
        if (!prop.isActive)
        {
            timeSinceFire = 2f;
            return;
        }

        if (Input.GetButtonDown("Fire1") && bulletsShot < 2 && timeSinceFire > 0.1f)
        {
            Shoot();
        }
        timeSinceFire += Time.deltaTime;

        if (timeSinceFire > 2f && bulletsShot == 2)
        {
            bulletsShot = 0;
        }
    }

    private void Shoot()
    {
        timeSinceFire = 0f;
        bulletParticles.Play();
        fireParticles.Play();
        smokeParticles.Play();
        bulletsShot++;

        RaycastHit[] hits = Physics.SphereCastAll(prop.ownerCamera.transform.position, 2f, prop.ownerCamera.transform.forward, maxDistance, playerLayerMask);
        foreach(var hit in hits)
        {
            var rb = hit.rigidbody;
            var isOwner = prop.owner && prop.owner.GetComponent<Rigidbody>() == rb;

            if (rb)
            {
                rb.AddForce(1000f * rb.mass * prop.ownerCamera.transform.forward * (isOwner ? -1f : 1f));
            }
        }
    }
}
