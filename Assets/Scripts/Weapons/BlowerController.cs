using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowerController : IWeaponController
{
    [SerializeField] private ParticleSystem windParticles = null;

    private float maxDistance = 500f;
    private float timeSinceFire = 2f;

    void Update()
    {
        if (!prop.isActive)
        {
            timeSinceFire = 2f;
            return;
        }

        if (Input.GetButtonDown("Fire1") && timeSinceFire > 2f)
        {
            Shoot();
        }
        timeSinceFire += Time.deltaTime;
    }

    private void Shoot()
    {
        timeSinceFire = 0f;
        windParticles.Play();

        RaycastHit[] hits = Physics.SphereCastAll(prop.ownerCamera.transform.position, 2f, prop.ownerCamera.transform.forward, maxDistance, playerLayerMask);
        foreach(var hit in hits)
        {
            var rb = hit.rigidbody;
            if (prop.owner && prop.owner.GetComponent<Rigidbody>() == rb)
            {
                rb.AddForce(4000f * rb.mass * -prop.ownerCamera.transform.forward);
                continue;
            }

            if (rb)
            {
                rb.AddForce(15000f * rb.mass * prop.ownerCamera.transform.forward);
            }
        }
    }
}
