using System.Collections.Generic;
using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

public class PlayerMovement : NetworkBehaviour
{
    private const float groundDistance = 0.1f;

    [SerializeField] private float runSpeed = 20f;
    [SerializeField] private float jumpHeight = 18f;
    [SerializeField] private float slowedMultiplier = 0.4f;

    [SerializeField] private LayerMask groundMask = 0;

    private CharacterController controller = null;
    private Transform groundCheck = null;
    private List<Transform> ceilingChecks = new List<Transform>();
    private Transform orientation, capsule, head = null;

    private float playerHeight, playerScale;
    private float bodyToHeadDistance;
    private bool isCrouching = false;
    private bool isGrounded;
    private bool hasUsedCrouchInAir;
    private Inventory inventory;

    private Rigidbody rb;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            InitializePlayer();
        }
        else
        {
            GetComponent<CharacterController>().enabled = false;
        }
    }

    void Update()
    {
        if (!rb)
        {
            return;
        }

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        isCrouching = Input.GetButton("Crouch") || (isCrouching && !CanUncrouch());
        hasUsedCrouchInAir = hasUsedCrouchInAir && !isGrounded;

        ApplyMovement();
        
        // Jumping
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb.velocity = new Vector3(rb.velocity.x, jumpHeight, rb.velocity.z);
        }

        if (!Grappling.isGrappling)
        {
            AdjustCharacterHeight();

            // Slow down player if mid-air and crouching
            if (Input.GetButtonDown("Crouch") && !isGrounded && !hasUsedCrouchInAir)
            {
                hasUsedCrouchInAir = true;
                rb.velocity = rb.velocity / 4f;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!base.IsOwner)
        {
            return;
        }
        inventory.EnterCollision(collision);
    }

    private bool CanUncrouch()
    {
        foreach (Transform ceilingCheck in ceilingChecks)
        {
            // Check if there is a ceiling over the player 
            if (Physics.Raycast(ceilingCheck.position, ceilingCheck.up, playerScale / 1.02f))
            {
                return false;
            }
        }

        return true;
    }

    private void ApplyMovement()
    {
        float adjustedSpeed = isCrouching && isGrounded ? runSpeed * slowedMultiplier : runSpeed;
        float x = Input.GetAxisRaw("Horizontal") * adjustedSpeed;
        float z = Input.GetAxisRaw("Vertical") * adjustedSpeed;

        if (x != 0f && z != 0f)
        {
            x *= 0.7071f;
            z *= 0.7071f;
        }

        Vector3 move = orientation.transform.right * x + orientation.transform.forward * z;

        // If player is almost stationary, give slight acceleration to start movement faster
        if (rb.velocity.magnitude < runSpeed / 4f)
        {
            move *= 2f;
        }
        
        rb.AddForce(new Vector3(move.x, rb.velocity.y, move.z) - rb.velocity);
    }

    private void AdjustCharacterHeight()
    {
        float yScale = isCrouching ? playerHeight * 0.6f : playerHeight;
        float bodyHeight = head.transform.position.y - (isCrouching ? bodyToHeadDistance * 0.65f : bodyToHeadDistance);
        capsule.transform.localScale = new Vector3(capsule.transform.localScale.x, yScale, capsule.transform.localScale.z);
        capsule.transform.position = new Vector3(capsule.transform.position.x, bodyHeight, capsule.transform.position.z);
    }

    private void InitializePlayer()
    {
        controller = GetComponent<CharacterController>();
        rb = controller.GetComponent<Rigidbody>();
        var playerBody = this.transform.Find("PlayerTransform").Find("PlayerBody");
        capsule = playerBody.Find("Capsule");
        head = playerBody.Find("Head");
        groundCheck = capsule.Find("GroundCheck");
        orientation = playerBody.Find("PositionData").Find("Orientation");
        playerHeight = capsule.transform.localScale.y;
        playerScale = capsule.transform.lossyScale.y;
        bodyToHeadDistance = head.transform.position.y - capsule.transform.position.y;
        inventory = GetComponentInChildren<Inventory>();

        for (int i = 1; i < 6; i++)
        {
            var child = head.GetChild(i);
            ceilingChecks.Add(child);
        }
    }
}
