using System.Collections.Generic;
using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

public class Inventory : NetworkBehaviour
{
    public Transform playerCamera;
    public Rigidbody playerRigidbody;
    public LayerMask propLayer;

    private const float maxDistance = 5f;
    private float timeSinceLastPickup = 10f;
    private Prop previousProp = null;
    private int ignoredForRaycastMask;
    private Outline previousOutline;

    private Prop item = null;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            playerCamera = Camera.main.transform;
        }
    }

    void Update()
    {
        if (playerCamera == null)
        {
            return;
        }

        // Interact with external object
        bool hasLineOnObject = Physics.Raycast(playerCamera.position, playerCamera.forward, out RaycastHit hit, maxDistance, propLayer);
        bool hasDirectLineToObject = hasLineOnObject && CustomRaycast.HasDirectLine(playerCamera.position, playerCamera.forward, maxDistance, hit);

        Outline outline = null;
        Prop prop = null;
        if (hit.collider != null)
        {
            outline = hit.collider.gameObject.GetComponentInParent<Outline>();
            prop = hit.collider.gameObject.GetComponentInParent<Prop>();
        }

        if (hasDirectLineToObject)
        {
            if (outline && !prop.isActive)
            {
                previousOutline = outline;
                outline.OutlineColor = item == null ? Color.white : Color.yellow;
                outline.enabled = true;
            }

            if (Input.GetButtonDown("Interact"))
            {
                PickUp(prop);
            }
        }
        else
        {
            if (previousOutline != null && !previousOutline.Equals(outline))
            {
                previousOutline.enabled = false;
            }
        }

        if (Input.GetButtonDown("Drop Item"))
        {
            Drop();
        }
        timeSinceLastPickup += Time.deltaTime;
    }

   public void EnterCollision(Collision collision)
    {
        var prop = collision.gameObject.GetComponentInParent<Prop>();
        if (prop != null && prop.shouldPickupOnCollision && item == null)
        {
            PickUp(prop);
        }
    }

    public void PickUp(Prop prop)
    {
        if (timeSinceLastPickup < 1f && prop.gameObject.GetInstanceID() == previousProp.gameObject.GetInstanceID())
        {
            return;
        }

        if (item != null)
        {
            Drop();
        }

        prop.gameObject.transform.parent = this.transform;
        prop.isActive = true;
        prop.AddConstraints();
        prop.owner = playerRigidbody.gameObject;
        prop.ownerCamera = prop.owner.GetComponentInChildren<Camera>();
        item = prop;
    }

    public void Drop()
    {
        if (this.transform.childCount > 0 && item != null)
        {
            timeSinceLastPickup = 0f;
            item.owner = null;
            item.ownerCamera = null;
            item.isActive = false;
            item.RemoveConstraints();
            item.transform.SetParent(item.initalParentTransform);
            Rigidbody rb = item.GetComponent<Rigidbody>();
            rb.velocity = playerRigidbody.velocity;
            Vector3 throwDirection = playerCamera.forward.normalized + new Vector3(0f, 0.75f, 0f);
            rb.AddForce(500f * rb.mass * throwDirection);
            previousProp = item;
            item = null;
        }
    }

    public Prop GetItem()
    {
        return item;
    }
}
