﻿using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

public class Grappling : NetworkBehaviour
{
    static public bool isGrappling = false;

    private const float boostSpeed = 70f;
    private const float regenTime = 1f;

    [SerializeField] private CharacterController controller = null;
    [SerializeField] private LayerMask whatIsGrappleable = 0;
    [SerializeField] private Transform sphere, playerCamera, rope = null; 

    private bool canGrapple = false;
    private float maxDistance = 50f;
    private float timeSinceLastGrapple = 0f;

    private Vector3 direction;
    private Vector3 launchPoint;
    private Vector3 savedSpherePosition;
    private Rigidbody rb;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            playerCamera = Camera.main.transform;
            rb = controller.GetComponent<Rigidbody>();
        }
    }

    void Update()
    {
        if (playerCamera == null)
        {
            return;
        }

        timeSinceLastGrapple += Time.deltaTime;
        bool hasLineOnObject = Physics.Raycast(playerCamera.position, playerCamera.forward, out RaycastHit raycastHit, maxDistance, whatIsGrappleable);
        bool hasDirectLineToObject = hasLineOnObject && CustomRaycast.HasDirectLine(playerCamera.position, playerCamera.forward, maxDistance, raycastHit);

        if (hasLineOnObject || isGrappling)
        {
            if (timeSinceLastGrapple > regenTime && hasDirectLineToObject)
            {
                canGrapple = true;
                sphere.gameObject.SetActive(true);
            }

            if (!isGrappling)
            {
                sphere.position = raycastHit.point;
                savedSpherePosition = sphere.position;
            }
            else
            {
                sphere.position = savedSpherePosition;
            }
        }
        else
        {
            sphere.gameObject.SetActive(false);
            if (!isGrappling)
            {
                canGrapple = false;
            }
        }

        const float endOfGrappleDistance = 5f;
        float distanceToHitPoint = Vector3.Distance(transform.position, launchPoint);
        if ((Input.GetButtonUp("Grapple") || distanceToHitPoint < endOfGrappleDistance) && isGrappling)
        {
            EndGrapple();
        }
        else if (Input.GetButton("Grapple") && canGrapple)
        {
            if (isGrappling)
            {
                rope.gameObject.SetActive(true);
            }
            else
            {
                launchPoint = sphere.position;
                direction = (sphere.position - transform.position);
            }
            isGrappling = true;
            Grapple(sphere, distanceToHitPoint);
        }
    }

    void Grapple(Transform hitPoint, float distanceToHitPoint)
    {
        direction.Normalize();
        rope.LookAt(launchPoint);
        rope.position = (transform.position + launchPoint) / 2;
        rope.localScale = new Vector3(0.1f, 0.1f, distanceToHitPoint - 0.5f);
        rb.velocity = direction * boostSpeed;
    }

    void EndGrapple()
    {
        isGrappling = false;
        canGrapple = false;
        timeSinceLastGrapple = 0f;
        rope.gameObject.SetActive(false);
        sphere.gameObject.SetActive(false);
    }
}
