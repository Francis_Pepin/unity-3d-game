﻿using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

public class PlayerMouseLook : NetworkBehaviour
{
    [SerializeField] private float mouseSensitivity = 100f;
    [SerializeField] private Transform playerContainer, playerBody, capsule, playerHead, playerCamera = null;

    private float xRotation = 0f;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            Cursor.lockState = CursorLockMode.Locked;
            playerCamera = Camera.main.transform;
            playerCamera.transform.SetParent(playerBody);
            playerCamera.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            playerCamera.localPosition = playerHead.localPosition + new Vector3(0f, 0f, 0f);
        }
    }

    void Start()
    {
        Application.targetFrameRate = 144;
    }

    void Update()
    {
        if (playerCamera == null)
        {
            return;
        }

        float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * Time.deltaTime * mouseSensitivity;

        if (Mathf.Abs(mouseY) > 150f)
        {
            return;
        }

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -80f, 80f);

        playerCamera.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerHead.transform.localRotation = Quaternion.AngleAxis(xRotation, Vector3.right);
        playerContainer.RotateAround(capsule.transform.position, Vector3.up, mouseX);
    }
}
