using UnityEngine;
using UnityEngine.UI;

public class OutOfBounds : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.tag == "Player")
        {
            Destroy(other.transform.root.gameObject);
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
