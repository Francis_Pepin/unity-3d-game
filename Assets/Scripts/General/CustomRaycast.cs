using UnityEngine;

public class CustomRaycast
{
    public static bool HasDirectLine(Vector3 origin, Vector3 direction, float maxDistance, RaycastHit raycastHit)
    {
        int invisibleMask = 1 << LayerMask.NameToLayer("Invisible");
        int playerMask = 1 << LayerMask.NameToLayer("Player");
        int ignoredForRaycastMask = invisibleMask | playerMask;

        RaycastHit[] hits = Physics.RaycastAll(origin, direction, maxDistance, ~ignoredForRaycastMask);
        foreach (RaycastHit hit in hits)
        {
            bool isCloser = Vector3.Distance(origin, hit.point) < Vector3.Distance(origin, raycastHit.point);
            if (hit.collider != raycastHit.collider && isCloser)
            {
                return false;
            }
        }
        return true;
    }
}
