using UnityEngine;

public class Prop : MonoBehaviour
{
    public bool isActive = false;
    public bool shouldPickupOnCollision = true;
    public GameObject owner = null;
    public Camera ownerCamera = null;
    public string propName = "";
    public Transform initalParentTransform = null;

    private Transform relativePosition = null;
    private Animator anim = null;
    private Collider propCollider;
    private Rigidbody rb;


    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        propCollider = GetComponentInChildren<Collider>();
        rb = GetComponent<Rigidbody>();
        relativePosition = transform.Find("RelativePosition").gameObject.transform;
        initalParentTransform = transform.parent;
    }

    void Update()
    {
        UpdateColliderAndConstraints();

        if (anim)
        {
            anim.enabled = isActive;
        }

        if (ownerCamera)
        {
            transform.localPosition = relativePosition.localPosition;
            transform.localEulerAngles = relativePosition.localEulerAngles;
            var playerRotationAxis = ownerCamera.transform.parent.transform.right;
            transform.RotateAround(ownerCamera.transform.position, playerRotationAxis, ownerCamera.transform.eulerAngles.x);
        }
    }

    private void UpdateColliderAndConstraints()
    {
        if (propCollider)
        {
            propCollider.enabled = !isActive;
        }

        if (rb)
        {
            if (isActive)
            {
                AddConstraints();
            }
            else
            {
                RemoveConstraints();
            }
        }
    }

    public void AddConstraints()
    {
        rb.constraints = RigidbodyConstraints.FreezeRotationX |
        RigidbodyConstraints.FreezeRotationY |
        RigidbodyConstraints.FreezeRotationZ |
        RigidbodyConstraints.FreezePositionX |
        RigidbodyConstraints.FreezePositionY |
        RigidbodyConstraints.FreezePositionZ;
    }

    public void RemoveConstraints()
    {
        rb.constraints = RigidbodyConstraints.None;
    }
}
