﻿using UnityEngine;
using UnityEngine.UI;

public class HealthSystem : MonoBehaviour
{
    public int maxHP = 100;
    public int currentHP = 100;
    public Slider slider;
    public Gradient gradient;
    public Image fill;
    public Text healthText;

    void Start()
    {
        slider.minValue = 0;
        slider.maxValue = maxHP;
        SetUI();
        currentHP = maxHP;
    }

    public void Damage(int damage)
    {
        currentHP -= damage;

        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }

        if (currentHP <= 0)
        {
            currentHP = 0;
        }

        SetUI();
    }

    public bool IsDead()
    {
        return currentHP <= 0;
    }

    public void Heal(int healthAmount)
    {
        Damage(-healthAmount);
    }

    public void Kill()
    {
        currentHP = 0;
        SetUI();
    }

    public void SetHP(int healthAmount)
    {
        currentHP = healthAmount;
        SetUI();
    }

    private void SetUI()
    {
        slider.value = currentHP;
        fill.color = gradient.Evaluate(slider.normalizedValue);
        healthText.text = $"{currentHP}/{maxHP}";
    }
}
